package com.example.register_task

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var mail: EditText
    private lateinit var password: EditText
    private lateinit var repeatPassword: EditText
    private lateinit var registerBtn: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialization()

    }

    private fun initialization() {
        mail = findViewById(R.id.mailBar)
        password = findViewById(R.id.passwordBar)
        repeatPassword = findViewById(R.id.repeatPasswordBar)
        registerBtn = findViewById(R.id.registerBtn)


        registerBtn.setOnClickListener {
            if (validateMail() && validatePassword() && validateRepeatPassword()) {
                val email = mail.text.toString()
                val password = password.text.toString()

                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "registered successfully", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, ProfileActivity::class.java))
                            finish()
                        } else {
                            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }

    private fun validateMail(): Boolean {
        if (mail.text.isEmpty()) {
            Toast.makeText(this, "mail bar is empty", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!mail.text.contains("@")) {
            Toast.makeText(this, "mail doesn't contains @ ", Toast.LENGTH_SHORT).show()
            return false
        }
        if (mail.text.last() == '.') {
            Toast.makeText(this, "mail is ending with ' . '", Toast.LENGTH_SHORT).show()
            return false
        }
        val email = mail.text.toString().split("@")
        if (email.size > 2) {
            Toast.makeText(this, "mail contains more then one '@'", Toast.LENGTH_SHORT).show()
            return false

        }
        if (email[0].length < 5) {
            Toast.makeText(
                this,
                "mail(first part) contains less then 6 letter",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (email[1].length < 6) {
            Toast.makeText(
                this,
                "mail(second part) contains less then 6 letter",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (!email[1].contains(".")) {
            Toast.makeText(
                this,
                "mail(second part) doesn't contains '.' ",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        return true
    }

    private fun validatePassword(): Boolean {
        if (password.text.isEmpty()) {
            Toast.makeText(this, "password bar is empty", Toast.LENGTH_SHORT).show()
            return false}

        if (password.text.length < 9) {
            Toast.makeText(this, "password < 9", Toast.LENGTH_SHORT).show()
            return false
        }
        var digit: Boolean = false
        var letter: Boolean = false
        for (element in password.text){
            val char = element
            if(char.isDigit()){
                digit = true
            }
            if(char.isLetter()){
                letter = true
            }
            if(digit && letter){
                return true
            }
        }
        Toast.makeText(this, "missing digits or letters", Toast.LENGTH_SHORT).show()
        return false
    }

    private fun validateRepeatPassword(): Boolean {
        if (password.text.toString() != repeatPassword.text.toString()) {
            Toast.makeText(this, "Passwords doesn't match!", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }
}